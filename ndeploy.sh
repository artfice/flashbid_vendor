#!/bin/bash

folder=$1
cd functions
find . -name "node_modules" -exec rm -rf '{}' +
rm -rf "./${folder}/ioContainer.js"
rm -rf "./${folder}/service"
rm -rf "./${folder}/model"
rm -rf "./${folder}/.env"
cd ..
cp -R ../flashbid/node_modules "./functions/${folder}/node_modules"
cp ../flashbid/ioContainer.js "./functions/${folder}/ioContainer.js"
cp -R ../flashbid/service "./functions/${folder}/service"
cp -R ../flashbid/model "./functions/${folder}/model"
cp .env "./functions/${folder}/.env"
apex deploy $1