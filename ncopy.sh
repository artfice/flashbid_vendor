#!/bin/bash

folder=$1

rm -rf "./functions/${folder}/node_modules"
rm -rf "./functions/${folder}/ioContainer.js"
rm -rf "./functions/${folder}/service"
rm -rf "./functions/${folder}/model"
rm -rf "./functions/${folder}/.env"

cp -R ../flashbid/node_modules "./functions/${folder}/node_modules"
cp ../flashbid/ioContainer.js "./functions/${folder}/ioContainer.js"
cp -R ../flashbid/service "./functions/${folder}/service"
cp -R ../flashbid/model "./functions/${folder}/model"
cp .env "./functions/${folder}/.env"