'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var PaginationService = require('./service/PaginationService');
/*
Steps:
1. Check user is distrubutor
2. If he is, pull items he owns
 */
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    
    const vendorId = event.principalId;

    iocContainer.models.items.findAll({
         where: {
            $or: [{
                status: 'Active'
            }, {
                status: 'Flash'
            }, {
                status: 'Complete'
            }, {
                status: 'Claimed'
            }],
            vendor_id: vendorId
        }        
    }).then(
        function (items) {
            callback(null, items);
        }).catch(function (e) {
            console.log('Items GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Items not found'
            });
        });
}
