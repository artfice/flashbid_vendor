'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');

exports.handle = function (event, context, callback) {
    const id = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body;
    const first_name = body && body.first_name ? body.first_name : '';
    const last_name = body && body.last_name ? body.last_name : '';
    const email = body && body.email ? body.email : '';
    const password = body && body.password ? body.password : '';
    const phone_number = body && body.phone_number ? body.phone_number : 0;
    let updateFields = {};

    if (first_name.length > 0) {
        updateFields.first_name = first_name;
    }
    if (last_name.length > 0) {
        updateFields.last_name = last_name;
    }
    if (email.length > 0) {
        updateFields.email = email;
    }
    if (phone_number > 0) {
        updateFields.phone_number = phone_number;
    }

    if (password.length > 0) {
        updateFields.password = securityService.hash(password);
    }

    iocContainer.models.users.find({
        where: {
            id: id
        }
    }).then(function (user) {
        return user.updateAttributes(updateFields);
    }).then(function (user) {
        user = user.toJSON();
        delete user.password;
        delete user.access_token;
        return callback(null, user);
    }).catch(function (e) {
        console.log('User EDIT ERROR', e);
        if (e.name == 'SequelizeUniqueConstraintError') {
            callback(null, {
                statusCode: 403,
                message: 'Este correo electronico ya existe en nuestro sistema.'
            });
        } else {
            callback(null, {
                statusCode: 403,
                message: 'Error en la actualización del perfil'
            });
        }
    });
};
