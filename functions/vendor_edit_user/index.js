'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.path.id;
    const first_name = body && body.first_name ? body.first_name : '';
    const last_name = body && body.last_name ? body.last_name : '';
    const email = body && body.email ? body.email : '';
    const password = body && body.password ? body.password : '';
    const phone_number = body && body.phone_number ? body.phone_number : 0;
    let updateFields = {};
  
    if (first_name.length > 0) {
        updateFields.first_name = first_name;
    }
    if (last_name.length > 0) {
        updateFields.last_name = last_name;
    }
    if (email.length > 0) {
        updateFields.email = email;
    }
    if (phone_number > 0) {
        updateFields.phone_number = phone_number;
    }

    if (password.length > 0) {
        updateFields.password = securityService.hash(password);
    }

    iocContainer.models.users.find({
        where: {
            id: id,
            role: 'Member'
        }
    }).then(function (user) {
        return user.updateAttributes(updateFields);
    }).then(function (user) {
        user = user.toJSON();
        delete user.access_token;
        delete user.refresh_token;
        delete user.confirm_token;
        delete user.reset_token;
        delete user.term_condition;
        delete user.password;        
        return firebaseService.write('user', user.id, user).then(function (result) {
            return callback(null, user);
        })
    }).catch(function (e) {
        console.log('User POST ERROR', e);
        callback(e);
    });
};
