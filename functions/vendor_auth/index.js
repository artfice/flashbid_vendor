'use strict';
var dotenv = require('dotenv');
dotenv.config();
var SecurityService = require('./service/SecurityService');
const securityService = new SecurityService();

function generatePolicy (principalId, event, effect, resource) {
    let authResponse = {
        principalId : principalId,
        context: event
    };
    if (effect && resource) {
        let policyDocument = {};
        policyDocument.Version = '2012-10-17'; // default version
        policyDocument.Statement = [];
        let statementOne = {};
        statementOne.Action = 'execute-api:Invoke'; // default action
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }
    return authResponse;
}


exports.handle = function (event, context, cb) {
    // {
    //     "type":"TOKEN",
    //     "authorizationToken":"<Incoming bearer token>",
    //     "methodArn":"arn:aws:execute-api:<Region id>:<Account id>:<API id>/<Stage>/<Method>/<Resource path>"
    // }
    const authorization = event.authorizationToken ? event.authorizationToken : false;
    const resource = event.methodArn ? event.methodArn : '';
    const apiOptions = {};
    const tmp = resource.split(':');
    if (tmp.length > 4) {
        const apiGatewayArnTmp = tmp[5].split('/');
        const awsAccountId = tmp[4];
        apiOptions.region = tmp[3];
        apiOptions.restApiId = apiGatewayArnTmp[0];
        apiOptions.stage = apiGatewayArnTmp[1];
    }

    context.callbackWaitsForEmptyEventLoop = false;

    console.log('Authorization', authorization);
    try {
        if (!authorization) {
            throw new Error('Authorization Token Missing');
        }
        const payload = securityService.verifyAccessToken(authorization);

        if (!payload) {
            throw new Error('Authorization Token Invalid');            
        }

        if (payload.role == 'Member') {
            throw new Error('Authorization Invalid Role');            
        }           
        // console.log('Generated Policy', JSON.stringify(generatePolicy(payload.id, payload.role, 'Allow', resource)));
        cb(null, generatePolicy(payload.id, event, 'Allow', resource));
    } catch (e) {
        console.log('Authorization Error ', e);
        cb('Unauthorized');
    }
}

// {
//   "principalId": "xxxxxxx", // the principal user identification associated with the token send by the client
//   "policyDocument": { // example policy shown below, but this value is any valid policy
//     "Version": "2012-10-17",
//     "Statement": [
//       {
//         "Effect": "Allow",
//         "Action": [
//           "execute-api:Invoke"
//         ],
//         "Resource": [
//           "arn:aws:execute-api:us-east-1:xxxxxxxxxxxx:xxxxxxxx:/test/*/mydemoresource/*"
//         ]
//       }
//     ]
//   }
// }