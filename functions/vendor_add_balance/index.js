'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var FirebaseService = require('./service/FirebaseService');
var SecurityService = require('./service/SecurityService');
var firebaseService = new FirebaseService();
var securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const vendorUserId = event.principalId;
    const id = event.body.id;
    const amount = event.body && event.body.amount != undefined ? event.body.amount : 0;
    const code = securityService.generateString(16);

    if (!id || amount < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Solicitud no válida'
        });
    }

    let currentUser = null;
    let currentBidCard = null;
    let currentSystem = null;
    let userBalance = 0;
    let moneyOwed = 0;
    let newUserBalance = 0;
    let newMoneyOwed = 0;
    let difference = 0;

    const today = new Date();
    const todayStr = today.toISOString().slice(0, 10);

    let noMoreBidCards = false;
    let bannedUser = false;

    iocContainer.db.transaction({
        isolationLevel: iocContainer.isolationLevel
    }).then(function (t) {
        return iocContainer.models.users.find({
            where: {
                id: id
            }
        }, { transaction: t }).then(
            function (user) {

        currentUser = lodash.clone(user);
        userBalance = lodash.clone(user.balance);
        moneyOwed = lodash.clone(user.money_owed);
        if (moneyOwed > 0 && userBalance <= 0) {
            //Scenarios
            //1. if user balance - moneyOwed  == negative
            newMoneyOwed = moneyOwed - amount;
            // -5 add 10 -> 5
            // -2 add 1 -> -1
            if (newMoneyOwed <= 0) {
                newUserBalance = Math.abs(newMoneyOwed);
                newMoneyOwed = 0;
            } else {
                //newMoney is still negative

            }
        } else {
            newMoneyOwed = moneyOwed - amount;
            // -5 add 10 -> 5
            // -2 add 1 -> -1
            if (newMoneyOwed <= 0) {
                newUserBalance = userBalance + Math.abs(newMoneyOwed);
                newMoneyOwed = 0;
            } else {
                //newMoney is still negative
                newUserBalance = userBalance;
            }
        }        

        if (user.status == 'Suspend' || user.status == 'Ban') {
            bannedUser = true;
            throw new Error('User is Suspended or Banned');
        }

        return user.updateAttributes({
            balance: newUserBalance,
            money_owed: newMoneyOwed
        }, { transaction: t });

    }).then(function (updatedUser) {

        return firebaseService.write('user', updatedUser.id,
            securityService.firebaseUserObject(updatedUser)).then(function (result) {
                return iocContainer.models.system.find({
                    where: {
                        id: 1
                    }
                }, { transaction: t });
            });

    }).then(function (system) {
        //STEP 2: Save Transaction
        currentSystem = lodash.clone(system);

        return iocContainer.models.transactions.create({
            action: 'UserBoughtCard',
            user_id: id,
            item_id: 0,
            amount: amount,
            userOldTotal: userBalance,
            userTotal: newUserBalance,
            systemOldTotal: currentSystem.earned,
            systemTotal: currentSystem.earned,
            code: code,
            notes: 'Usuario ' + id + ' compro una recarga "Flash" en Vendedor Flash Autorizado  ' + vendorUserId + ' de ' + amount,
            created_at: todayStr,
            updated_at: today
        }, { transaction: t });
    }).then(function (transaction) {
        return iocContainer.models.transactions.create({
            action: 'VendorCredit',
            user_id: vendorUserId,
            item_id: 0,
            amount: amount,
            userOldTotal: 0,
            userTotal: 0,
            systemOldTotal: 0,
            systemTotal: 0,
            code: code,
            notes: 'Usuario ' + id + ' compro una recarga "Flash" en Vendedor Flash Autorizado ' + vendorUserId + ' de ' + amount,
            created_at: todayStr,
            updated_at: today
        }, { transaction: t });
    }).then(function (transaction) {
        return iocContainer.models.system.find({
            where: {
                id: 1
            }
        }, { transaction: t });
    }).then(function (system) {
        //STEP 3: Update System Balance
        return system.updateAttributes({
            money_owed: system.money_owed + amount
        }, { transaction: t });
    }).then(function (system) {
        return iocContainer.models.users.find({
            where: {
                id: vendorUserId
            }
        }, { transaction: t });
    }).then(function (user) {
        return user.updateAttributes({
            money_owed: user.money_owed + amount
        }, { transaction: t });
    }).then(function (user) {
        return iocContainer.models.history.create({
            user_id: id,
            message: 'Recarga "Flash" de $ ' + amount + ' fue añadida a su cuenta',
            read: 0,
            created_at: todayStr,
            updated_at: Math.ceil(today.getTime() / 10000)
        }, { transaction: t });
    }).then(function (user) {
        t.commit();
        return callback(null, {
            statusCode: 200,
            message: 'Recarga "Flash" de $ ' + amount + ' fue añadida a su cuenta'
        });
    }).catch(function (err) {
        console.log('User Credit Added Error ', err);
        t.rollback();
        if (bannedUser) {
            return callback(null, {
                statusCode: 403,
                message: 'Usuario suspendido o prohibido. No puedes agregarle recarga o crédito.'
            });
        } else {
            return callback(null, {
                statusCode: 403,
                message: 'No se pudo agregar crédito'
            });
        }
    });
});
}
