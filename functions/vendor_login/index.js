'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var lodash = require('lodash');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;
    const email = body && body.email ? body.email : null;
    const password = body && body.password ? body.password : null;

    let accessToken = '';
    let refreshToken = '';

    if (!email || email.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'Ingrese un correo electrónico Valido'
        });
    }
    if (!password || password.length < 1) {
        return callback(null, {
            statusCode: 403,
            message: 'se requiere contraseña'
        });
    }

    iocContainer.models.users.find({
        where: {
            email: email
        }
    }).then(function (user) {

        const oldPassword = user.password;

        if (!securityService.compareHash(oldPassword, password)) {
            return {
                statusCode: 403,
                message: 'Contraseña no reconocida.'
            };
        } else if (user.status == 'Suspend') {
            return {
                statusCode: 403,
                message: 'Tu cuenta está suspendida.'
            };
        } else if (user.status == 'Ban') {
            return {
                statusCode: 403,
                message: 'Su cuenta está prohibida.'
            };
        } else if (user.status == 'Pending') {
            return {
                statusCode: 403,
                message: 'Confirmar su cuenta antes de iniciar sesión.'
            };
        } else if (user.role == 'Member') {
            throw new Error('[401] Unauthorized Access');
        } else {
            accessToken = securityService.createAccessToken({
                id: user.id,
                role: user.role,
                status: user.status
            }, process.env.ACCESS_TOKEN_EXPIRE_SECOND);

            return user.updateAttributes({
                access_token: accessToken
            });
        }
    }).then(function (user) {
        if (user.statusCode) {
            return callback(null, user);
        } else {
            return callback(null, {
                access_token: accessToken,
                user_id: user.id
            });
        }
    }).catch(function (e) {
        console.log('Member Login GET ERROR', e);
        return {
            statusCode: 403,
            message: 'credenciales no válidas'
        };
    });
};