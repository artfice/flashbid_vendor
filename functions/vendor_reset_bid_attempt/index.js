'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const securityService = new SecurityService();
    const body = event.body;
    const id = event.query.id;
    if (!id) {
        return callback(null,{
            statusCode: 403,
            message: 'User not found'
        });
    }

    iocContainer.models.users.find({
        where: {
            id: id,
            role: 'Member'
        }
    }).then(function (user) {
        return user.updateAttributes({
            bid_attempts: 0
        });
    }).then(function (user) {
        var user = user.toJSON();
        delete user.access_token;
        delete user.refresh_token;
        delete user.confirm_token;
        delete user.reset_token;
        delete user.term_condition;
        delete user.role;
        delete user.password;        
        return firebaseService.write('user', user.id, user).then(function (result) {
            return callback(null, user);
        })
    }).catch(function (e) {
        console.log('User POST ERROR', e);
        callback(e);
    });
};
