'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var SecurityService = require('./service/SecurityService');
var securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const vendorId = event.principalId;
    const id = event.path.id;
    const where = {
        id: id,
        role: 'Member'
    };
    let payload = {};
    iocContainer.models.users.find({ where: where }).then(
        function (user) {
            payload.user = securityService.firebaseUserObject(user);
            return iocContainer.models.history.findAll({
                where: {
                    user_id: id
                }
            })
        }).then(function (history) {
            if (history) {
                payload.history = history;
            } else {
                payload.history = [];
            }
            return iocContainer.models.items.findAll({
                where: {
                    winner_id: id
                }
            });
        }).then(function (items) {
            if (items) {
                payload.items = items;
            } else {
                payload.items = [];
            }
            return iocContainer.models.transactions.findAll({
                where: {
                    user_id: id
                }
            });
        }).then(function (transactions) {
            if (transactions) {
                payload.transactions = transactions;
            } else {
                payload.transactions = [];
            }
            return callback(null, payload);
        }).catch(function (e) {
            console.log('Get GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'No se ha encontrado la información del usuario'
            });
        });
}
