'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var SNSService = require('./service/SNSService');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
var Promise = require('bluebird');

const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const userId = event.principalId;
    const id = event.query.id;
    const winner_id = event.query.winner_id;
    const code = event.query.code;

    const today = new Date();
    const todayStr = today.toISOString().slice(0, 10);
    let notComplete = false;
    let state = '';

    if (!id) {
        return callback(null, {
            statusCode: 403,
            message: 'Objeto no encontrado'
        });
    }

    let promiseList = [
        iocContainer.models.items.find({ where: { id: id } }),
        iocContainer.models.users.find({ where: { id: userId } }),
        iocContainer.models.users.find({ where: { id: winner_id } })
    ];

    Promise.all(promiseList).then(function (results) {
        const item = results[0];
        const vendor = results[1];
        const user = results[2];

        if (item.status != 'Complete') {
            state = 'NotComplete';
            throw new Error('Item is not in complete state');
        }

        if (item.vendor_id != vendor.id) {
            state = 'NotVendorItem';
            throw new Error('Item is not under vendor');
        }

        if (item.winner_id != user.id) {
            state = 'NotWinner';
            throw new Error('User did not with the item');
        }

        if (item.winner_code != code) {
            state = 'NotWinner';
            throw new Error('User did not with the item');
        }

        if (user.money_owed > 0) {
            state = 'MoneyOwed';
            throw new Error('You have outstanding balance on your account');
        }

        return item.updateAttributes({
            status: 'Claimed'
        });
    }).then(function (item) {
        return iocContainer.models.history.create({
            user_id: item.winner_id,
            message: 'Usted reclamó el articulo ' + item.title + ' en una tienda distribuidora.',
            read: 0,
            created_at: todayStr,
            updated_at: Math.ceil(today.getTime() / 10000)
        });

    }).then(function (history) {
        const snsService = new SNSService('refreshItems');
        return snsService.deliverMessage({}).then(function (snsMessage) {
            return callback(null, {
                statusCode: 200,
                message: 'Artículo reclamàdo'
            });
        }).catch(function (e) {
            return callback(null, {
                statusCode: 200,
                message: 'Artículo reclamàdo'
            });
        });
    }).catch(function (e) {
        if (state == 'NotComplete') {
            return callback(null, {
                statusCode: 403,
                message: 'Lo sentimos, la subasta por el artículo todavia està abierta.'
            });
        } else if (state == 'NotVendorItem') {
            return callback(null, {
                statusCode: 403,
                message: 'Este artículo no puede ser reclamado con este distribuidor'
            });
        } else if (state == 'MoneyOwed') {
            return callback(null, {
                statusCode: 403,
                message: 'El usuario debe depositar el balance completo antes de reclamar el premio.'
            });
        } else if (state == 'NotWinner') {
            return callback(null, {
                statusCode: 403,
                message: 'Este usuario no es el ganador.'
            });
        } else if (state.length < 1) {
            return callback(null, {
                statusCode: 403,
                message: 'Lo sentimos, este usuario no puede reclamar el premio en este momento.'
            });
        }
    })
};
